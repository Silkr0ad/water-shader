Shader "Custom/Water"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _NormalTex ("Normap Map", 2D) = "bump" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _ScrollXSpeed1 ("X Scroll Speed 1", Range(-10, 10)) = 0
        _ScrollYSpeed1 ("Y Scroll Speed 1", Range(-10, 10)) = 0
        _ScrollXSpeed2 ("X Scroll Speed 2", Range(-10, 10)) = 0
        _ScrollYSpeed2 ("Y Scroll Speed 2", Range(-10, 10)) = 0
        _Scale ("Second Normal Map Scale", Range(0, 1)) = 0.25
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _NormalTex;
        fixed _ScrollXSpeed1;
        fixed _ScrollYSpeed1;
        fixed _ScrollXSpeed2;
        fixed _ScrollYSpeed2;
        fixed _Scale;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_NormalTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed xOffset1 = _ScrollXSpeed1 * _Time;
            fixed yOffset1 = _ScrollYSpeed1 * _Time;
            fixed2 UVOffset = fixed2(xOffset1, yOffset1);

            fixed xOffset2 = _ScrollXSpeed2 * _Time;
            fixed yOffset2 = _ScrollYSpeed2 * _Time;
            fixed2 secondUVOffset = fixed2(xOffset2, -yOffset2);

            fixed2 mainUV = IN.uv_MainTex + UVOffset;
            fixed2 normalUV = IN.uv_NormalTex + UVOffset;
            fixed2 secondNormalUV = IN.uv_NormalTex + secondUVOffset;
            
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, mainUV) * _Color;
            o.Albedo = c.rgb;

            // Calculate the normal vector.
            float4 normalPixel = tex2D(_NormalTex, normalUV);
            float3 n = UnpackNormal(normalPixel);
            float4 secondNormalPixel = tex2D(_NormalTex, secondNormalUV * _Scale);
            float3 n2 = UnpackNormal(secondNormalPixel);

            float3 finalNormal = normalize(float3(n.rg + n2.rg, n.b * n2.b));
            o.Normal = finalNormal.xyz;
            
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
