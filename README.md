# Simple Water Shader

A simple water shader using a normal map to achieve its look. Those tend to appear flat in VR, but surprisingly, this one turned out alright.

A normal texture is layered twice. Each is scaled a by a different amount, and each moves in a different direction at a different speed. The result is a deceptively realistic illusion of water—though much of that realism depends on the normal texture used.

The material and texture used to achieve the look below are included in the repo.

![Water Shader GIF](media.gif "Water Shader")